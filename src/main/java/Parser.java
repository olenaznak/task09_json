import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import model.Gun;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Parser {
    public static void main (String[] args) {
        File jsonFile = new File("src\\main\\resources\\gunJSON.json");
        File jsonSchemeFile = new File("src\\main\\resources\\gunJSONScheme.json");

        try {
            if (JSONValidator.validate(jsonFile, jsonSchemeFile)) {
                JSONParser parser = new JSONParser();
                List<Gun> guns = parser.getGuns(jsonFile);
                guns.stream().sorted(new GunComparator()).map(Gun::toString).forEach(System.out::println);
            } else {
                System.out.println("Not valid!");
            }
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
    }
}
